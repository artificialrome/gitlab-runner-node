# dockerfile to build the image artificialrome will use to run most of our build processes.
FROM node:7.1.0-alpine
MAINTAINER Dennis Timmermann <d@tmrmn.com>
RUN apk update

# we need gulp every time anyway.
RUN npm install -g gulp-cli
